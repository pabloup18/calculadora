package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private TextView display;
    private TextView display_resultado;
    private TextView display_conta;

    ArrayList<String> numeros = new ArrayList<String>();
    ArrayList<String> operacoes = new ArrayList<String>();
    private String numero = "";
    private float resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        display = findViewById(R.id.tv_display);
        display_resultado = findViewById(R.id.tv_resultado);
        display_conta = findViewById(R.id.tv_conta);
    }
    public void OnClickBtnBtnIgual(View v){
        numeros.add(numero);
        if (numeros.size() == 0 || numeros.size() == 1){
            display.setText("");
            clear_cache();
        } else {
            resultado = Integer.parseInt(numeros.get(0));
            for (int i = 0; i < operacoes.size(); i++) {
                if(operacoes.get(i).equals("+")){
                    resultado = resultado + Float.parseFloat(numeros.get(i + 1));
                }
                if(operacoes.get(i).equals("-")){
                    resultado = resultado - Float.parseFloat(numeros.get(i + 1));
                }
                if(operacoes.get(i).equals("*")){
                    resultado = resultado * Float.parseFloat(numeros.get(i + 1));
                }
                if(operacoes.get(i).equals("/")){
                    resultado = resultado / Float.parseFloat(numeros.get(i + 1));
                }
            }
            display_resultado.setText(Float.toString(resultado));
            display_conta.setText(display.getText().toString());
            display.setText("");
            clear_cache();
            resultado = 0;
        }
    }
    public void OnClickBtnBtnClear(View v){
        clear_cache();
        display.setText("");
        display_resultado.setText("0");
        display_conta.setText("0");
    }
    public void OnClickBtnBtnAdicao(View v){
        if (!numero.isEmpty()) {
            numeros.add(numero);
            operacoes.add("+");
            AtualizaDisplay("+");
            numero = "";
        }else{
            Toast.makeText(MainActivity.this, "DIGITE UM NUMERO ANTES", Toast.LENGTH_LONG).show();
        }
    }
    public void OnClickBtnBtnSubtracao(View v){
        if (!numero.isEmpty()) {
            numeros.add(numero);
            operacoes.add("-");
            AtualizaDisplay("-");
            numero = "";
        }else{
            Toast.makeText(MainActivity.this, "DIGITE UM NUMERO ANTES", Toast.LENGTH_LONG).show();
        }
    }
    public void OnClickBtnBtnDivisao(View v){
        if (!numero.isEmpty()) {
            numeros.add(numero);
            operacoes.add("/");
            AtualizaDisplay("/");
            numero = "";
        }else{
            Toast.makeText(MainActivity.this, "DIGITE UM NUMERO ANTES", Toast.LENGTH_LONG).show();
        }
    }
    public void OnClickBtnBtnMultiplicacao(View v){
        if (!numero.isEmpty()) {
            numeros.add(numero);
            operacoes.add("*");
            AtualizaDisplay("*");
            numero = "";
        }else{
            Toast.makeText(MainActivity.this, "DIGITE UM NUMERO ANTES", Toast.LENGTH_LONG).show();
        }
    }
    public void OnClickBtnBtn0(View v){
        numero = numero + "0";
        AtualizaDisplay("0");
    }
    public void OnClickBtnBtn1(View v){
        numero = numero + "1";
        AtualizaDisplay("1");
    }
    public void OnClickBtnBtn2(View v){
        numero = numero + "2";
        AtualizaDisplay("2");
    }
    public void OnClickBtnBtn3(View v){
        numero = numero + "3";
        AtualizaDisplay("3");
    }
    public void OnClickBtnBtn4(View v){
        numero = numero + "4";
        AtualizaDisplay("4");
    }
    public void OnClickBtnBtn5(View v){
        numero = numero + "5";
        AtualizaDisplay("5");
    }
    public void OnClickBtnBtn6(View v){
        numero = numero + "6";
        AtualizaDisplay("6");
    }
    public void OnClickBtnBtn7(View v){
        numero = numero + "7";
        AtualizaDisplay("7");
    }
    public void OnClickBtnBtn8(View v){
        numero = numero + "8";
        AtualizaDisplay("8");
    }
    public void OnClickBtnBtn9(View v){
        numero = numero + "9";
        AtualizaDisplay("9");
    }
    public void AtualizaDisplay(String texto){
        String TextoDisplay = display.getText().toString();
        TextoDisplay = TextoDisplay + texto;
        display.setText(TextoDisplay);
    }
    public void clear_cache(){
        numeros.clear();
        operacoes.clear();
        numero = "";
    }

}